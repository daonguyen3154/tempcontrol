void SENSOR_ReadData(void)
{
    // this function read data from sensor
    sensor_raw_data = ReadSensor();
}

void SENSOR_CalculateData(void)
{
    // this function calculate sensor data value to get temperature
    sensor_calculated_data = CalculatedSensor();
}
