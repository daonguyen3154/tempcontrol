/*
    This function control relay based on the calculcated temperature and set temperature.
    Current temperature is displayed in this mode.
*/
void IO_OperationModeControl(void)
{
    if (sensor_calculated_data < (temperature_set_value + delta))
    {
        RELAY_TURN_ON();
    }
    if (sensor_calculated_data > (temperature_set_value - delta))
    {
        RELAY_TURN_OFF();
    }
    display_number = sensor_calculated_data;
}
/*
    This function sets the set value by using 2 buttons UP and DOWN.
    Set temperature is displayed in this mode.
*/
void IO_UserSetupModeControl(void)
{
    if (detect UP_BUTTON Rising edge)
    {
        temperature_set_value++;
    }
    if (detect DOWN_BUTTON Rising edge)
    {
        temperature_set_value--;
    }
    display_number = temperature_set_value;
}

/*
    This function auto calculate PID paramters Kp, Ki, Kd.
*/
void PID_StartTunning()
{
    // Calculate Kp, Ki, Kd.
}
