void Peripheral_Setup()
{
}

void Run_Interface()
{
    switch (state)
    {
    case OperationMode:

    case UserSetupMode:

        switch (sub_state)
        {
        case TempSet:

        case PIDSet:
        }

    case PIDAutoTunning:

    default:
        break;
    }
}

void Run_Control()
{
    switch (state)
    {
    case ExecuteState:

    case IDLEState:

    case ErrorExectuteMode:

    default:
        break;
    }
}

void task_10ms()
{
    Run_Interface();
}

void task_100ms()
{
    Run_Control();
}

void main()
{
    Peripheral_Setup();
    while (1)
    {
        task_10ms();
        task_100ms();
    }
}