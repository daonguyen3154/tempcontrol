typedef enum error_type
{
    NO_ERROR = 0,
    ERROR_TYPE_SENSOR_LOST ,
    ERROR_TYPE_OVER_TEMPERATURE
}error_type_t;

void ERROR_Check(void)
{
    if((sensor_raw_data < SENSOR_RAW_MIN_THRESHOLD))
    {
        type_of_error = ERROR_TYPE_SENSOR_LOST;
    }
    else if(sensor_calculated_data > MAX_TEMPERATURE)
    {
        type_of_error = ERROR_TYPE_OVER_TEMPERATURE;
    }
    else 
    {
        type_of_error = NO_ERROR;
    }
}
